FROM node:16 AS base
ADD . /app
WORKDIR /app

FROM base as prod-install
WORKDIR /app
RUN npm ci --only=production

FROM base as prod-build
WORKDIR /app
COPY --from=prod-install /app/node_modules /app/node_modules
RUN npm ci
RUN npm run build

FROM gcr.io/distroless/nodejs:16
WORKDIR /app
COPY --from=base /app /app
COPY --from=prod-install /app/node_modules /app/node_modules
COPY --from=prod-build /app/.nuxt /app/.nuxt

EXPOSE 3000
ENV HOST=0.0.0.0
ENV PORT=3000

CMD [ "node_modules/.bin/nuxt", "start" ]
