# CHANGELOG

## 1.4.0 (2022-06-27)
### FEATURES
- "Accompagnement sur mesure" page
- "Incubateurs locaux" page
- Add links to new pages
- Dropdown menu for offer pages
- Emojis to prefix test on home page cards
### FIX
- Publication date for job offers
- Typos wording and ui fixes

## 1.3.0 (2022-05-13) 
### FEATURES
- 404 custom page
### FIX
- Mobile video display for about page
- Feedback-widget position
- Mobile offer page title
- Redirect incorrect slugs to their parent

## 1.2.2 (2022-05-11)
### FIX
- Mobile header navigation
- Mobile feedback-widget

## 1.2.1 (2022-05-10)
### FIX
- Pagination previous and next navigation
- Forum link in offer page

## 1.2.0 (2022-05-09)
### FEATURES
- Page "details de services"
- Tracking on search
- Search bar on landing page

### FIX
- Search input
- UI hotfixes
- Refactor card component

## 1.1.1 (2022-04-26)
### FEATURES
- Page "services" provide sharable filter with query param
- Track contact button click on page "service"
- Replace under construction by a link button on page "offres" 
### FIX
- Typos
- Filter behavior
- Conflicts between SSR and client rendering
- Remove JS DSFR
- Special item in nav z-index
- Card links

## 1.1.0 (2022-04-19)
### FEATURES
- Page "a propos"
- Page "services"
- Meilisearch plugin
### FIX
- Contact form
- Remove subtitle in offer page
- UI fix on link button

## 1.0.5 (2022-03-28)
### FEATURES
- Matomo setup
- LogRocket setup

## 1.0.4 (2022-03-25)
### FEATURES
- Pages "actualites"
- Created Vue components to embed dsfr

## 1.0.3 (2022-03-16)
### FEATURES
- Authenticated api connection

## 1.0.2 (2022-03-15)
### FEATURES
- Pages "investigation"

## 1.0.1 (2022-03-14)
### FIX
- Favicon updated

## 1.0.0 (2022-03-11)
### FEATURES
- Page home
- Page "mentions legales"
- Pages "recrutement"
- Page "contact"
- Page "donnees personnelles"
- Page "offre"
- Plan du site
- Connection with backend api
- General nuxt setup
