import { MeiliSearch } from 'meilisearch';

export const INDEX = 'services';
export const FILTERABLE_SERVICE_FIELDS = [
  'territorial_level_target',
  'resource_type',
  'territory_themes',
  'incubator_program',
  'service_status',
];

export default ({ $config }, inject) => {
  const meilisearchClient = new MeiliSearch({
    host: $config.meilisearchHost,
    apiKey: $config.meilisearchApiKey,
  });

  inject('search', meilisearchClient);
};
