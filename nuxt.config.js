const API_URL = process.env.API_URL || 'http://localhost:8080';
const MEILISEARCH_HOST =
  process.env.MEILISEARCH_HOST || 'http://localhost:7700';
const { API_TOKEN, MEILISEARCH_API_KEY } = process.env;

export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  telemetry: false,

  head: {
    title: 'Incubateur des Territoires',
    htmlAttrs: {
      lang: 'fr',
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    script: [
      {
        src: 'https://incubateur-territoires.github.io/feedback-widget/widget.js',
      },
    ],
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~/assets/style/main.css'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [{ src: '~/plugins/http.js' }, { src: '~/plugins/meilisearch.js' }],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    '@nuxtjs/axios',
    ['nuxt-matomo', { matomoUrl: '//stats.data.gouv.fr/', siteId: 135 }],
    'nuxt-logrocket',
  ],

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},

  publicRuntimeConfig: {
    apiURL: API_URL,
    apiToken: API_TOKEN,
    meilisearchHost: MEILISEARCH_HOST,
    meilisearchApiKey: MEILISEARCH_API_KEY,
  },

  logRocket: {
    logRocketId: 'xz8jmz/incubateuranctgouvfr',
  },
};
